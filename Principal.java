package org.inguelberth.sistema;

import org.inguelberth.utilidades.Entrada;
import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.app.AppAdmin;
import org.inguelberth.app.AppEmpleado;
import org.inguelberth.app.AbstractAppRol;

public class Principal{
	public void iniciar(){
		do{
			AbstractAppRol app = null;		


			String nick, password;
			System.out.println("Ingrese Nick:");
			nick=Entrada.getInstancia().leer();
			System.out.println("Ingrese Password:");
			password=Entrada.getInstancia().leer();
		
			boolean resultado = ManejadorUsuario.getInstancia().autenticarUsuario(nick, password);

			if(resultado){
				System.out.println("Bienvenido "+ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getNombre());
				switch(ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getRol()){
					case "admin":
						app=new AppAdmin(new Decodificador());
						
						break;
					case "empleado":
						app=new AppEmpleado(new Decodificador());
						
						break;
					default:
						System.out.println("Nuestro sistema esta en mantenimiento, su rol no existe. :(");
				}
				app.iniciar();
			}else
				System.out.println("Verifique sus credenciales.");
		}while(true);
	}
}
