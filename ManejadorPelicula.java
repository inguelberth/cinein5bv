package org.inguelberth.manejadores;

import java.util.ArrayList;

import org.inguelberth.beans.Pelicula;
import org.inguelberth.beans.Anuncio;

public class ManejadorPelicula{
	private static ManejadorPelicula instancia;
	private ArrayList<Pelicula> listaPelicula;

	public ManejadorPelicula(){
		this.listaPelicula=new ArrayList<Pelicula>();
	}
	public ArrayList<Pelicula> obtenerLista(){
		return this.listaPelicula;
	}
	public void agregarPelicula(Pelicula pelicula){
		this.listaPelicula.add(pelicula);
	}
	public void eliminarPelicula(Pelicula pelicula){
		this.listaPelicula.remove(pelicula);
	}
	public Pelicula buscarPelicula(String nombre){
		for(Pelicula pelicula : this.obtenerLista()){
			if(pelicula.getNombre().equals(nombre))
				return pelicula;
		}
		return null;
	}
	public Anuncio buscarDentroDeAnuncio(Anuncio anuncio, String nombre){
		for(Anuncio anuncioDentro : anuncio.getListaAnuncio()){
			if(anuncioDentro.getNombre().equals(nombre))
				return anuncioDentro;
		}
		return null;
	}

	public static ManejadorPelicula getInstancia(){
		if(instancia==null)
			instancia=new ManejadorPelicula();
		return instancia;
	}
	

}
