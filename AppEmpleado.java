package org.inguelberth.app;

import java.util.HashMap;

import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.utilidades.eventos.DecodeListener;

public class AppEmpleado extends AbstractAppRol implements DecodeListener{

	public AppEmpleado(Decodificador decodificador){
		decodificador.addDecodeListener(this);	
		super.setDecodificador(decodificador);
		super.setConnected(true);
	}
	
	public void iniciar(){                    
		super.iniciar();
	}
	public void avisarAccionar(String accion, HashMap<String, String> parametros){
		super.revisarAccionar(accion, parametros);
	}
}
