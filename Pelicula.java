package org.inguelberth.beans;

import java.util.ArrayList;

import org.inguelberth.enumeration.Genero;

public class Pelicula extends Anuncio{
	private Genero genero;
	private int calificacion;

	
	public void setGenero(Genero genero){
		this.genero=genero;
	}
	public Genero getGenero(){
		return this.genero;
	}
	public void setCalificacion(int calificacion){
		this.calificacion=calificacion;
	}
	public int getCalificacion(){
		return this.calificacion;
	}
	public Pelicula(){
		super();
	}
}
