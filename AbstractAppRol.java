package org.inguelberth.app;

import java.util.HashMap;

import org.inguelberth.utilidades.Entrada;
import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.manejadores.ManejadorPelicula;
import org.inguelberth.beans.Pelicula;
import org.inguelberth.beans.Anuncio;

public abstract class AbstractAppRol{
	private Decodificador decodificador;
	private boolean estadoSesion;

	public Decodificador getDecodificador(){
		return this.decodificador;
	}
	public void setDecodificador(Decodificador decodificador){
		this.decodificador=decodificador;
	}
	public boolean isConnected(){
		return this.estadoSesion;
	}
	public void setConnected(boolean estadoSesion){
		this.estadoSesion=estadoSesion;
	}
	

	public void buscarPelicula(){
		
	}
	public void obtenerPeliculas(){

	}
	public void obtenerSalas(){
		
	}
	public String obtenerLineas(int nivel){
		String resultado="";
		for(int posicion=0;posicion<nivel;posicion++){
			resultado+="     ";
		}
		return resultado;
	}
	
	public void listarAnuncios(Anuncio anuncio, int nivel){
		String numeroDeLineas=this.obtenerLineas(nivel);
		for(Anuncio anuncioDentro : anuncio.getListaAnuncio()){
			System.out.println(numeroDeLineas+"-----------------------");
			System.out.println(numeroDeLineas+"|_____Nombre: "+anuncioDentro.getNombre());
			System.out.println(numeroDeLineas+"|_____Patrocinador: "+anuncioDentro.getPatrocinador());
			System.out.println(numeroDeLineas+"|_____Segundos: "+String.valueOf(anuncioDentro.getSegundos()));
			this.listarAnuncios(anuncioDentro, nivel+1);
		}
		System.out.println(numeroDeLineas+"-----------------------");
	}

	public void revisarAccionar(String accion, HashMap<String,String> parametros){
		switch(accion.trim()){
			case "list movies":
				for(Pelicula pelicula : ManejadorPelicula.getInstancia().obtenerLista()){
					System.out.println("---------------------------------");
					System.out.println("Nombre: "+pelicula.getNombre());
					System.out.println("Genero: "+pelicula.getGenero().toString());
					System.out.println("Calificacion: "+String.valueOf(pelicula.getCalificacion()));
					System.out.println("--------ANUNCIOS-------");
					this.listarAnuncios(pelicula, 1);
				}
				System.out.println("---------------------------------");
			break;
		}
	}

	public void iniciar(){
		do{
			System.out.print("comandos>>");
			String comando=Entrada.getInstancia().leer();
			if(this.decodificador!=null){
				this.decodificador.decodificarComando(comando);
			}
		}while(this.isConnected());
	}
}
