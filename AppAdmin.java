package org.inguelberth.app;

import org.inguelberth.utilidades.eventos.DecodeListener;
import org.inguelberth.utilidades.Entrada;
import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.manejadores.ManejadorPelicula;
import org.inguelberth.beans.Usuario;
import org.inguelberth.beans.Pelicula;
import org.inguelberth.beans.Anuncio;
import org.inguelberth.enumeration.Genero;

import java.util.HashMap;

public class AppAdmin extends AbstractAppRol implements DecodeListener{

	public AppAdmin(Decodificador decodificador){
		decodificador.addDecodeListener(this);	
		super.setDecodificador(decodificador);
		super.setConnected(true);
	}
	
	public void iniciar(){                    
		super.iniciar();
	}
	public void avisarAccionar(String accion, HashMap<String, String> parametros){
		switch(accion.trim()){
			case "add user":
				Usuario usuario = new Usuario();

				String nombre=parametros.get("nombre");
				usuario.setNombre(nombre);

				usuario.setNick(parametros.get("nick"));
				usuario.setPassword(parametros.get("password"));
				usuario.setRol(parametros.get("rol"));
				usuario.setEdad(Integer.parseInt(parametros.get("edad")));
	
				ManejadorUsuario.getInstancia().agregarUsuario(usuario);

				System.out.println("	Usuario agregado satisfactoriamente. :)");
				
				break;
			case "remove user":
				Usuario usuarioAEliminar = ManejadorUsuario.getInstancia().buscarUsuario(parametros.get("nick"));
				if(usuarioAEliminar!=null){
					ManejadorUsuario.getInstancia().eliminarUsuario(usuarioAEliminar);
					System.out.println("	Usuario "+parametros.get("nick")+" eliminado satisfactoriamente.");
				}else{
					System.out.println("	El usuario no existe :$");
				}
				break;
			case "list user":
				for(Usuario user : ManejadorUsuario.getInstancia().obtenerListaUsuario()){
					System.out.println("-------------------------------");
					System.out.println("nombre: "+user.getNombre());
					System.out.println("nick: "+user.getNick());
					System.out.println("rol: "+user.getRol());
					System.out.println("edad: "+user.getEdad());
					System.out.println("-------------------------------");
				}
				System.out.println("-------------------------------");
				System.out.println("-------------------------------");
				System.out.println("Fin de la lista");
				System.out.println("-------------------------------");
				break;
			case "add movie":
				Pelicula pelicula = new Pelicula();
				pelicula.setNombre(parametros.get("nombre"));
				pelicula.setCalificacion(Integer.parseInt(parametros.get("calificacion")));
				pelicula.setPatrocinador(parametros.get("patrocinador"));
				pelicula.setSegundos(Integer.parseInt(parametros.get("segundos")));
				switch(parametros.get("genero")){
					case "terror":
						pelicula.setGenero(Genero.Terror);
						break;
					case "comedia":
						pelicula.setGenero(Genero.Comedia);
						break;
					case "romance":
						pelicula.setGenero(Genero.Romance);
						break;
				}
				ManejadorPelicula.getInstancia().agregarPelicula(pelicula);
				break;
			case "add advert":
				Anuncio anuncio = new Anuncio();
				anuncio.setNombre(parametros.get("nombre"));
				anuncio.setPatrocinador(parametros.get("patrocinador"));
				anuncio.setSegundos(Integer.parseInt(parametros.get("segundos")));
				
				String[] ruta = parametros.get("down").split(",");
				Pelicula peliR = ManejadorPelicula.getInstancia().buscarPelicula(ruta[0]);
				Anuncio anuncioResultado = peliR;
				
				for(int posicion=1;posicion<ruta.length;posicion++){
					anuncioResultado=ManejadorPelicula.getInstancia().buscarDentroDeAnuncio(anuncioResultado, ruta[posicion]);
				}
				//System.out.println("      ESTA ES LA PELI "+anuncio.getNombre()+"   dentro de "+anuncioResultado.getNombre());
				anuncioResultado.getListaAnuncio().add(anuncio);
			break;
			case "exit user":
				super.setConnected(false);
				break;
			default:
				super.revisarAccionar(accion, parametros);
				System.out.println("Compruebe su sintaxis");
		}
		
	}
}
